import Button from 'react-bootstrap/Button';
import { useState } from 'react';
import ModalCustom from './ModalCustom';

const ButtonCustom = (props) => {
    const [modalShow, setModalShow] = useState(false);

    function clickHandler(){
        if(props.data.age <= 0 || props.data.username === ""){
            setModalShow(true);
        }else{
            props.function(props.data)
        }
    }

    return(
      <div>
            <Button onClick={ clickHandler } variant="primary">Primary</Button>
            <ModalCustom show={modalShow} onHide={() => setModalShow(false)} />
      </div>
    );
}

export default ButtonCustom;