import { useState } from 'react';
import FormInput from './FormInput';
import Display from './Display';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import './Login.css';

const Login = () => {
    let [list, setList] = useState([]);

    function addNewData(payload){
        console.log(payload)
        setList([
            ...list, payload
        ])
        console.log(list);
    }

    return(
        <Container className="mt-5">
           <Row>
                <Col>
                    <Card>
                        <Card.Body>
                            <FormInput function={ addNewData }/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row className="mt-3">
                <Col>
                    <Card>
                        <Card.Body>
                        <Display data={ list }/>    
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default Login;