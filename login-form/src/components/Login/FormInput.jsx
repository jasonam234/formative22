import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form'
import { useState } from 'react';
import ButtonCustom from './ButtonCustom';
import './Form.css'

const FormInput = (props) => {
    let [list, setList] = useState(
        {
            username:"",
            age:0
        }
    );

    function usernameHandler(events){
        console.log(events.target.value)
        setList({
            ...list, username: events.target.value
        });
    }

    function ageHandler(events){
        setList({
            ...list, age: events.target.value
        });
    }

    return(
            <Container className="mt-4">
                <Row className="mt-2">
                    <Col xs="auto">
                        <div>
                            <p>Username</p>
                        </div>
                    </Col>
                    <Col></Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form>
                        <Form.Group className="mb-3" onChange={ usernameHandler } controlId="exampleForm.ControlInput1">
                            <Form.Control type="text" placeholder="username" />
                        </Form.Group>
                        </Form>
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col xs="auto">
                        <div>
                            <p>Age (Years)</p>
                        </div>
                    </Col>
                    <Col></Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form>
                            <Form.Group className="mb-3" onChange={ ageHandler } controlId="exampleForm.ControlInput1">
                                <Form.Control type="number" placeholder="age" />
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <div>
                            <ButtonCustom  data={ list } function={props.function}/>
                        </div>
                    </Col>
                </Row>
            </Container>
    );
}

export default FormInput;