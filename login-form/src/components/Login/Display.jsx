import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import './Display.css';

const Display = (props) => {
    // return(
    //     <div className="custom-background">
    //         <Container>
    //             {
    //                 props.data.map((value, index) =>{
    //                     return(
    //                         <Row key={index}>
    //                             <Col>
    //                                 <div>
    //                                     {`${value.username} (${value.age})`}
    //                                 </div>
    //                             </Col>
    //                         </Row>
    //                     );
    //                 })
    //             }
    //         </Container>
    //     </div>

    // );

    const columns = [{
        dataField: 'username',
        text: 'Username'
      }, {
        dataField: 'age',
        text: 'Age'
      }];

      return(
        <BootstrapTable keyField='id' data={ props.data } columns={ columns } pagination={ paginationFactory() }/>
      );

      
      
}

export default Display;